<?php

namespace core;
use core\FiguresInterfaces;

/**
 * Geometric figure square
 * @version 1.0
 * @author Carlos Gutierrez
 */
class Square implements FiguresInterfaces {
    private $side; 
    
    /**
     * Method construct
     */
    function __construct ($property) {
        $this->side = $property->side;
    }

    /**
     * Method Type
     * @return string
     */
    public function getType() {
        return 'Square';
    }

    /**
     * Method for calculating the area of a circle
     * @return number
     */
    public function getArea() {
        if($this->side > 0) {
            return $this->side * $this->side;
        } else {
            return 0; 
        }
    }

    /**
     * Method for calculating the base of a square
     * @return number
     */
    public function getBase() {
        if($this->side > 0) {
            return $this->side;
        } else {
            return 0; 
        }
    }

    /**
     * Method for calculating the height of a square
     * @return number
     */
    public function getHeight() {
        if($this->side > 0) {
            return $this->side;
        } else {
            return 0; 
        }
    }

    /**
     * Method for calculating the diameter of a square
     * @return string
     */
    public function getDiameter() {
        return 'null';
    }
}



?>
<?php

namespace core;
use core\FiguresInterfaces;

/**
 * Geometric figure circle
 * @version 1.0
 * @author Carlos Gutierrez
 */
class Circle implements FiguresInterfaces {
    private $radius; 

    /**
     * Method construct
     */
    function __construct ($property) {
        $this->radius = $property->radius;
    }

    /**
     * Method Type
     * @return string
     */
    public function getType() {
        return 'Circle';
    }

    /**
     * Method for calculating the area of a circle
     * @return number
     */
    public function getArea() {
        if($this->radius > 0) {
            return (3.14 * pow($this->radius,2));
        } else {
            return 0; 
        }
    }

    /**
     * Method for calculating the area of a circle
     * @return string
     */
    public function getBase() {
        return 'null';
    }

    /**
     * Method for calculating the area of a circle
     * @return string
     */
    public function getHeight() {
        return 'null';
    }
    
    /**
     * Method for calculating the diameter of a circle
     * @return number
     */
    public function getDiameter() {
        return $this->radius * 2;
    }
}

?>
<?php

namespace core;

/**
 * Factory of geometric figures
 * @version 1.0
 * @author Carlos Gutierrez
 */
class FiguresFactory {

    public function createFigure($type, $property = null) {
        switch ($type) {
            case 'square':
                return new Square($property);
                break;
            case 'triangle':
                return new Triangle($property);
                break;
            case 'circle':
                return new Circle($property);
                break;
            default:
                # code...
                break;
        }
    }
}


?>
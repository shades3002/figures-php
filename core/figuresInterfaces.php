<?php

namespace core;

/**
 * Interface of geometric figures
 * @version 1.0
 * @author Carlos Gutierrez
 */
interface FiguresInterfaces {
    public function getArea();
    public function getBase();
    public function getHeight();
    public function getDiameter();
    public function getType();    
}

?>
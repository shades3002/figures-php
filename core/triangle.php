<?php

namespace core;
use core\FiguresInterfaces;

/**
 * Geometric figure Ttriangle
 * @version 1.0
 * @author Carlos Gutierrez
 */
class Triangle implements FiguresInterfaces {
    private $base; 
    private $height;
    
    /**
     * Method construct
     */
    function __construct ($property) {
        $this->base = $property->base;
        $this->height = $property->height;
    }

    /**
     * Method Type
     * @return string
     */
    public function getType() {
        return 'Triangle';
    }

    /**
     * Method for calculating the area of a triangle
     * @return number
     */
    public function getArea() {
        if($this->base > 0 && $this->height > 0) {
            return ($this->base * $this->height) / 2;
        } else {
            return 0; 
        }
    }

    /**
     * Method for calculating the base of a triangle
     * @return number
     */
    public function getBase() {
        return ($this->getArea() * 2) / $this->height;
    }

    /**
     * Method for calculating the height of a triangle
     * @return number
     */
    public function getHeight() {
        return $this->height;
    }
    
    /**
     * Method for calculating the diameter of a triangle
     * @return string
     */
    public function getDiameter() {
        return 'null';
    }
}

?>
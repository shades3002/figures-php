# MODELING OF CLASSES - Figures

# Features
* PHP

# Motivation

Perform the class modeling exercise by implementing the factory pattern

# Installation

Note: follow the steps to see in operation

1. Clone the project 
```
$ git clone git@gitlab.com:shades3002/figures-php.git
```
2. Install:
```
$ cd figuresPhp
$ composer install
```
3. Run:
```
$ php -S localhost:9090 -t .
```
4. Open Browser:
[http://localhost:9090](http://localhost:9090)

# Contributors
- Carlos Gutierrez
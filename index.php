<?php

require __DIR__ . '/vendor/autoload.php';

use core\FiguresFactory;

//Data
$property = new \stdClass();
$property->radius = 2;
$property->side = 3;
$property->base = 4;
$property->height = 4;

//factory
$figure = new FiguresFactory;

echo '<h3>MODELING OF CLASSES - Figures</h3>';

//circle
$circle = $figure->createFigure('circle', $property);

echo '<span><strong>Type:</strong> ' . $circle->getType() .'</span><br>';
echo '<span><strong>Area:</strong> ' . $circle->getArea() .'</span><br>';
echo '<span><strong>Base:</strong> ' . $circle->getBase() .'</span><br>';
echo '<span><strong>Height:</strong> ' . $circle->getHeight() .'</span><br>';
echo '<span><strong>Diameter:</strong> ' . $circle->getDiameter() .'</span><br>';
echo '<br><hr>';

//square
$square = $figure->createFigure('square', $property);

echo '<span><strong>Type:</strong> ' . $square->getType() .'</span><br>';
echo '<span><strong>Area:</strong> ' . $square->getArea() .'</span><br>';
echo '<span><strong>Base:</strong> ' . $square->getBase() .'</span><br>';
echo '<span><strong>Height:</strong> ' . $square->getHeight() .'</span><br>';
echo '<span><strong>Diameter:</strong> ' . $square->getDiameter() .'</span><br>';
echo '<br><hr>';

//triangle
$triangle = $figure->createFigure('triangle', $property);

echo '<span><strong>Type:</strong> ' . $triangle->getType() .'</span><br>';
echo '<span><strong>Area:</strong> ' . $triangle->getArea() .'</span><br>';
echo '<span><strong>Base:</strong> ' . $triangle->getBase() .'</span><br>';
echo '<span><strong>Height:</strong> ' . $triangle->getHeight() .'</span><br>';
echo '<span><strong>Diameter:</strong> ' . $triangle->getDiameter() .'</span><br>';
echo '<br><hr>';
echo '<h3>Class Diagram</h3>';
echo '<div style="text-align:center"><img src="resources/Class _Diagram.jpg" width="500" /></div>';










?>